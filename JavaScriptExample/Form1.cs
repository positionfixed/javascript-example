﻿using BrowseEmAll.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JavaScriptExample
{
    public partial class Form1 : Form
    {
        private BrowserManager manager = new BrowserManager();

        private const string search_Term = "BrowseEmAll Core API";

        private bool executed = false;

        private int browserId;

        public Form1()
        {
            InitializeComponent();

            this.FormClosed += Form1_FormClosed;

            manager.OnBrowserReady += Manager_OnBrowserReady;
            manager.OnDocumentComplete += Manager_OnDocumentComplete;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            manager.CloseBrowser(browserId);
        }

        private void Manager_OnDocumentComplete(object sender, BrowseEmAll.API.Events.DocumentCompletedEventArgs e)
        {
            if (!executed)
            {
                // Enter the search term into the search box and hit search
                executed = true;
                manager.ExecuteJavaScript(e.BrowserID, "document.getElementById('lst-ib').value = '" + search_Term + "';document.getElementsByTagName('form')[0].submit();");
            }
        }

        private void Manager_OnBrowserReady(object sender, BrowseEmAll.API.Events.BrowserReadyEventArgs e)
        {
            browserId = e.BrowserID;

            // Browser is ready so navigate to google
            manager.Navigate(e.BrowserID,"http://www.google.de");
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            btnGo.Enabled = false;
            executed = false;

            // Create the browser and add it to the form
            Control browser = (Control)manager.OpenBrowser(Browser.CHROME48, new BrowseEmAll.API.Models.BrowserSettings());
            browser.Dock = DockStyle.Fill;
            panelBrowser.Controls.Add(browser);

        }
    }
}
